<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pagar</title>
        <link rel="stylesheet" href="{{ asset('/css/validationEngine.jquery.css') }}">
        <style type="text/css">
			.input, label{
				position: relative;
			}
			.input{
				margin: 20px 0;
			}
			label{
				display: inline-block;
				vertical-align: top;
			}
			input{
				width: 200px;
			}
			#card-form{
				display: block;
				max-width: 480px;
				margin: 20px auto;
				padding-top: 100px;
			}
        </style>        
    </head>
    <body>
        
		<form action="{{ route('pagar') }}" method="POST" id="card-form">
			<span class="card-errors"></span>
			<div class="input">
				<label>
					<span>Nombre del tarjetahabiente</span><br>
					<input type="text" id="data-nombre" size="20" data-conekta="card[name]" class="validate[required]">
				</label>
			</div>
			<div class="input">
				<label>
					<span>Número de tarjeta de crédito</span><br>
					<input type="text" id="data-num" size="20" data-conekta="card[number]" class="validate[required]">
				</label>
			</div>
			<div class="input">
				<label>
					<span>CVC</span><br>
					<input type="text" id="data-cvc" size="4" data-conekta="card[cvc]" class="validate[required]">
				</label>
			</div>
			<div class="input">
				<span>Fecha de expiración (MM/AAAA)</span><br>
				<label>
					<input type="text" id="data-fecha-mes" size="2" data-conekta="card[exp_month]" class="validate[required]">
				</label>
				<label>
					<span>/</span>
					<input type="text" id="data-fecha-year" size="4" data-conekta="card[exp_year]" class="validate[required]">
				</label>
			</div>
			<button type="submit">Crear token</button>
		</form>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
		<script type="text/javascript" src="{{ asset('/js/jquery.validationEngine.js') }}"></script>
		<script type="text/javascript" src="{{ asset('/js/jquery.validationEngine-es.js') }}"></script>
		<script type="text/javascript">

			Conekta.setPublishableKey('{{Config::get("packages/conekta/config.public_key")}}');

			var conektaSuccessResponseHandler = function(token) {
				var $form = $("#card-form");
				//Inserta el token_id en la forma para que se envíe al servidor
				$form.append($('<input name="token" type="hidden" id="conektaTokenId">').val(token.id));
				$form.get(0).submit(); //Hace submit
			};

			var conektaErrorResponseHandler = function(response) {
			
				var $form = $("#card-form");
				$form.find(".card-errors").text(response.message_to_purchaser);
				$form.find("button").prop("disabled", false);
			};

			//jQuery para que genere el token después de dar click en submit
			$(function () {
				$("#card-form").submit(function(event) {

					var valid = $(this).validationEngine('validate');				
					console.log(valid);
					if (valid == true) {				
					
						var $form = $(this);
						// Previene hacer submit más de una vez
						$form.find("button").prop("disabled", true);
						Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
					}else{
						$(this).validationEngine();						
					}

					return false;


				});
			});

			@if(Session::get('mensaje') != null)
				alert("{{Session::get('mensaje')}}");
			@endif
		</script>
    </body>
</html>