<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function Home()
	{
		return View::make('home');
	}


	public function Pagar()
	{
		try {
			
			$token = Input::get('token');

			\Conekta\Conekta::setApiKey(Config::get("packages/conekta/config.private_key"));
		    \Conekta\Conekta::setApiVersion("2.0.0");	        			        		

		    //Array de productos
		    $line_items = array();

		    //Simulamos tener 5 productos
		    for ($i=0; $i <5; $i++) 
		    {

		    	$producto = array(
					"name" => "Modelo 000-".$i,
					//El precio se tiene que multiplicar por 100, es una especificación de conekta
					"unit_price" => 200 * 100,
					"quantity" => 1
				);

				array_push($line_items, $producto);
		    		
		    }
		    $cliente_array = array(
				"name" => "Javier Rios", //Cliente
				"email" => "hector@quimaira.com", //Correo del cliente
				"phone" => "4761020897", //Teléfono del cliente
				"payment_sources" => array(
					array(
			    		"type" => "card",
			    		"token_id" => $token
					)
				)
			);

		    $cliente_conekta_id = \Conekta\Customer::create($cliente_array);	   	  	   	

		    //Orden de conekta
		    $order_array = array(	        			
				"line_items" => $line_items,
				"shipping_lines" => array(
					array(
						"amount" => 80 * 100, //Costo de envío multiplicado por 100
						"carrier" => "Estafeta" //Paquetería
					)
				),
				"currency" => "MXN",    
				"customer_info" => array(
					"name" => "Javier Ríos", //Nombre del cliente
					"email" => 'hector@quimaira.com', //Email del cliente
					"phone" => '4761020897' //Teléfono del cliente
				),
				"shipping_contact" => array(
					"email" => 'hector@quimaira.com', //Email del quien recibe el producto (cliente)
					"phone" => '4761020897', //Teléfono de quien recibe el producto (cliente)
					"receiver" => "Javier Rios", //Persona que recibe el producto (cliente)
					"address" => array(							
						'street1'=> "Av. Panorama 314, Panorama",									
						'between_streets' => 'X, Y',
						'city'=> "León",
						'state'=> "Guanajuato",
						'zip'=> "36400",
						'country'=> 'México',
						'postal_code' => "37400",
						"residential" => true
					)
				),		        			
				'metadata'    => array('reference' => "1000001"), //folio o referencia de la compra
				"amount" => 1080 * 100, //total por 100 (total de procutos + envio)			
				"customer_info" => array(
					"customer_id" => $cliente_conekta_id->id //Cliente conekta id
				), //customer_info
				"charges" => array(
					array(
						"payment_method" => array(
							"payment_source_id" => $cliente_conekta_id->payment_sources[0]->id, //Id del pago con conekta
							"type" => "card"
						)
					)
				)
			);

			$orden = \Conekta\Order::create($order_array);
			
			if($orden->payment_status == 'paid')
			{
				$orden_conekta_id = $orden->charges[0]->order_id;
				return View::make('orden_pagada')->with('orden_conekta_id' ,$orden_conekta_id);
			}else{
				return Redirect::route('home')->with('mensaje', 'No se pudo pagar la orden de compra');
			}

		} catch (\Conekta\ErrorList $errorList) {
			$mensaje = '';
			foreach($errorList->details as $errorDetail) {
				$mensaje = $mensaje.' '.$errorDetail->getMessage();
			}					
    		
    		return Redirect::back()->with('mensaje', $mensaje);
		}
		

	}
}
