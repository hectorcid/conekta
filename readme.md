## Ejemplo de pago con Conekta 

Por el momento solo pongo el ejemplo de pago con tarjeta, posteriormente integraré el pago con OXXO y con SPEI.

En este ejemplo solo uso validación básica en el front con un plugin de jQuery.


## API Keys Conekta

Las variables de configuración están en /app/config/packages/conekta/config.php

Para el pago con tarjeta pueden porbar pagos "correctos" con el número: 4242424242424242
Nombre, CVC, y Fecha pueden ser datos ficticios.

Pueden encontrar más tarjeta de prueba y con diferentes estatus en:
```
https://developers.conekta.com/resources/testing
```

## Librerías usadas

conekta/conekta-php
```
https://github.com/conekta/conekta-php
```

Validation Engine (jQuery)
```
https://github.com/posabsolute/jQuery-Validation-Engine
```